import csv
import psycopg2


def main():
    conn = psycopg2.connect("host=localhost dbname=finalproject user=finalproject password=finalproject")
    conn.autocommit = True
    cur = conn.cursor()
    cur.execute('DROP SCHEMA IF EXISTS finalschema CASCADE')
    cur.execute('CREATE SCHEMA finalschema')
    # cur.execute('GRANT ALL PRIVILEGES ON SCHEMA finalschema TO finalproject')

    cur.execute('DROP TABLE IF EXISTS descriptions')
    cur.execute('DROP TABLE IF EXISTS apps')
    cur.execute('DROP TABLE IF EXISTS reviews')
    cur.execute('DROP TABLE IF EXISTS ratings')
    cur.execute('DROP TABLE IF EXISTS info')

    cur.execute("""
        CREATE TABLE descriptions (
            name VARCHAR(255) PRIMARY KEY,
            description VARCHAR(4096)
        )
    """)

    cur.execute("""
        CREATE TABLE apps (
            name VARCHAR(255),
            genre VARCHAR(255),
            PRIMARY KEY (name, genre),
            FOREIGN KEY (name) REFERENCES descriptions (name)
        )
    """)
    
    cur.execute("""
        CREATE TABLE reviews (
            name VARCHAR(255),
            user_review VARCHAR (4096),
            FOREIGN KEY (name) REFERENCES descriptions (name)
        )  
    """)

    cur.execute("""
        CREATE TABLE ratings (
            name VARCHAR(255),
            rating FLOAT,
            review_count BIGINT,
            content_rating VARCHAR(63),
            FOREIGN KEY (name) REFERENCES descriptions (name)
        )
    """)

    cur.execute("""
        CREATE TABLE info (
            name VARCHAR(255),
            version VARCHAR(63),
            size BIGINT,
            price MONEY,
            FOREIGN KEY (name) REFERENCES descriptions (name)
        )
    """)

    google_csv = "googleplaystore.csv"
    # ['App', 'Category', 'Rating', 'Reviews', 'Size', 'Installs', 
    # 'Type', 'Price', 'Content Rating', 'Genres', 'Last Updated', 
    # 'Current Ver', 'Android Ver']
    
    apple_csv = "AppleStore.csv"
    # ['', 'id', 'track_name', 'size_bytes', 'currency', 'price', 
    # 'rating_count_tot', 'rating_count_ver', 'user_rating', 
    # 'user_rating_ver', 'ver', 'cont_rating', 'prime_genre', 
    # 'sup_devices.num', 'ipadSc_urls.num', 'lang.num', 'vpp_lic']

    apple_desc_csv = "appleStore_description.csv"
    # ['id', 'track_name', 'size_bytes', 'app_desc']

    google_user_review_csv = "googleplaystore_user_reviews.csv"
    # ['App', 'Translated_Review', 'Sentiment', 'Sentiment_Polarity', 
    # 'Sentiment_Subjectivity']

    with open(google_csv, "r") as fd:
        csv_data = csv.reader(fd)
        row_counter = 0
        next(csv_data)
        for row in csv_data:
            row_counter += 1
            cur.execute(
                "INSERT INTO descriptions VALUES (%s, NULL) ON CONFLICT DO NOTHING",
                [row[0]]
            )
        print(f"{row_counter} rows inserted from Google to descriptions")
    
    with open(google_user_review_csv, "r") as fd:
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            cur.execute(
                "INSERT INTO descriptions VALUES (%s, NULL) ON CONFLICT DO NOTHING",
                [row[0]]
            )
        print(f"{row_counter} rows inserted from Google Reviews to descriptions")

    with open(apple_desc_csv, "r") as fd:
        csv_data = csv.reader(fd)
        row_counter = 0
        next(csv_data)
        for row in csv_data:
            description = row[3]
            row_counter += 1
            cur.execute(
                "INSERT INTO descriptions VALUES (%s, %s) ON CONFLICT DO NOTHING",
                [row[1], description]
            )
        print(f"{row_counter} rows inserted from Apple to descriptions")

    with open(google_csv, "r") as fd:
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            categories = set()
            categories.add(row[1].lower())
            genres = row[9].split(";")
            for genre in genres:
                genre = genre.replace("&", "and").replace(" ", "_").lower()
                categories.add(genre)
            for category in list(categories):
                row_counter += 1
                cur.execute(
                    "INSERT INTO apps VALUES (%s, %s) ON CONFLICT DO NOTHING",
                    [row[0], category]
                )
        print(f"{row_counter} rows inserted from Google to apps")
    
    # {'finance', 'business', 'book', 'medical', 'navigation', 
    # 'food_and_drink', 'health_and_fitness', 'sports', 'music', 
    # 'social_networking', 'games', 'travel', 'shopping', 'catalogs', 
    # 'weather', 'productivity', 'lifestyle', 'entertainment', 'news', 
    # 'education', 'reference', 'photo_and_video', 'utilities'}

    with open(apple_csv, "r") as fd:
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            genre = row[12].lower().replace("&", "and").replace(" ", "_")
            row_counter += 1
            cur.execute(
                "INSERT INTO apps VALUES (%s, %s) ON CONFLICT DO NOTHING",
                [row[2], genre]
            )
        print(f"{row_counter} rows inserted from Apple to apps")

    units = {"k": 10**3, "m": 10**6}
    with open(google_csv, "r") as fd:
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            if "m" in row[4].lower():
                size = float(row[4].lower().rstrip("m")) * units["m"]
                size = str(int(size))
            elif "k" in row[4].lower():
                size = float(row[4].lower().rstrip("k")) * units["k"]
                size = str(int(size))
            else:
                size = None

            if "$" in row[7]:
                money = float(row[7].lstrip("$"))
            elif row[7] == '0':
                money = float(row[7])
            else:
                money = None

            if row[11] == "Varies with device" or row[11] == "NaN":
                 version = None
            else:
                 version = row[11]
                            
            cur.execute(
                "INSERT INTO info VALUES (%s, %s, %s, %s)",
                 [row[0], version, size, money]
            )
        print(f"{row_counter} rows inserted from Google to info")

    with open(apple_csv, "r") as fd:
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            cur.execute(
                "INSERT INTO info VALUES (%s, %s, %s, %s)",
                 [row[2], row[10], row[3], row[5]]
            )
        print(f"{row_counter} rows inserted from Apple to info")

    with open(google_user_review_csv, "r") as fd:
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            cur.execute(
                'INSERT INTO reviews VALUES (%s, %s) ON CONFLICT DO NOTHING',
                [row[0], row[1]]
            )
        print(f"{row_counter} rows inserted from Google Reviews to reviews")

    with open(apple_csv, "r") as fd:
        csv_data = csv.reader(fd)
        row_counter = 0
        next(csv_data)
        for row in csv_data:
            row_counter += 1
            cur.execute(
                "INSERT INTO reviews VALUES (%s, NULL) ON CONFLICT DO NOTHING",
                [row[2]]
            )
        print(f"{row_counter} rows inserted from Apple to reviews")

    with open(google_csv, "r") as fd:
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            total = row[3].replace('M',"00000").replace('.',"")
            cur.execute(
                 'INSERT INTO ratings VALUES (%s, %s, %s, %s) ON CONFLICT DO NOTHING',
                 [row[0], row[2], total, row[8]]
            )
        print(f"{row_counter} rows inserted from Google to ratings")

    with open(apple_csv, "r") as fd:
        csv_data = csv.reader(fd)
        next(csv_data)
        row_counter = 0
        for row in csv_data:
            row_counter += 1
            total = row[3].replace('M',"00000").replace('.',"")
            cur.execute(
                'INSERT INTO ratings VALUES(%s, %s, %s, %s) ON CONFLICT DO NOTHING',
                [row[2], row[8], row[6], row[10]]
            )
        print(f"{row_counter} rows inserted from Apple to ratings")

if __name__ == "__main__":
    main()